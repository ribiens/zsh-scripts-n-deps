#!/bin/bash
en="\`@#\$\^&qwertyuiop\[\]QWERTYUIOPasdfghjkl;\'ASDFGHJKL|zxcvbnm,ZXCVBNM?./" 
ru="ё\"№;:?йцукенгшщзхъЙЦУКЕНГШЩЗфывапролджэФЫВАПРОЛД/ячсмитьбЯЧСМИТЬ,ю." 
led=$(xset -q|grep LED| awk '{ print $10 }') 

case $led in 
    00000000)  f=$en && s=$ru  ;; 
    00001000)  f=$ru && s=$en  ;; 
esac 

xclip -o -selection clipboard | sed y=$f=$s= | xclip -i -selection clipboard 
xkb-switch -n
