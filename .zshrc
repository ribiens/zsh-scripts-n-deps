source /home/rjotanm/zsh-git-prompt/zshrc.sh

if [[ -n "$SSH_CLIENT" || -n "$SSH2_CLIENT" ]]; then
    if [[ $EUID == 0 ]]; then
        PROMPT=$'%{\e[1;31m%}%#~ %{\e[0m%}' # root
    else
        PROMPT=$'%{\e[1;33m%}%~ %{\e[0m%}' # regular user
    fi
    # right prompt with hostname or bad smiley
    RPROMPT=$'%b$(git_super_status) # %(?,%{\e[34m%}ssh %n@%m%{\e[0m%},%{\e[1;31m%}:(%{\e[0m%})'

else # not SSH
    if [[ $EUID == 0 ]]; then
        PROMPT=$'%{\e[1;31m%}%#~ %{\e[0m%}' # root
    else
        PROMPT=$'%{\e[1;32m%}%~ %{\e[0m%}' # regular user
    fi
    # right prompt with hostname or bad smiley
    RPROMPT=$'%b$(git_super_status) # %(?,%{\e[34m%}%m%{\e[0m%},%{\e[1;31m%}:(%{\e[0m%})'
fi

alias scp='scp -r'
alias rm='rm -r'
alias mkdir='mkdir -p'

alias ls='ls -F --color=auto'
alias la='ls -A --color=auto'
alias ll='ls -l --color=auto -h'
alias lla='ll -A --color=auto -h'

alias grep='grep --colour=auto'

alias wake="echo $'\a'"

bindkey "^[[5~" history-beginning-search-backward # pg up
bindkey "^[[6~" history-beginning-search-forward  # pg dow


typeset -U path
path=(~/bin $path)

# setopt autocd      # change directory without typing cd
setopt correctall  # correct typos
setopt NO_BEEP

# enable completion
autoload -U compinit && compinit
#
# # use arrow keys to navigate completion suggestions
setopt menucomplete
zstyle ':completion:*' menu select=1 _complete _ignored _approximaten

# cd and ls in one shot
cdls() {
    local dir=$1
    test -z "$dir" && dir="$HOME"
    cd "$dir" && ls
}

cdll() {
    local dir=$1
    test -z "$dir" && dir="$HOME"
    cd "$dir" && ll
}

# classic archive extractor
extract () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2) tar xvjf $1   ;;
            *.tar.gz)  tar xvzf $1   ;;
            *.bz2)     bunzip2 $1    ;;
            *.rar)     unrar x $1    ;;
            *.gz)      gunzip $1     ;;
            *.tar)     tar xvf $1    ;;
            *.tbz2)    tar xvjf $1   ;;
            *.tgz)     tar xvzf $1   ;;
            *.zip)     unzip $1      ;;
            *.Z)       uncompress $1 ;;
            *.7z)      7z x $1       ;;
            *)         echo "'$1' cannot be extracted via $0" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# and packer
pk() {
    if [ $2 ]; then
        case $1 in
            tbz)    tar cjvf "$2.tar.bz2" "$2"    ;;
            tgz)    tar czvf "$2.tar.gz"  "$2"    ;;
            tar)    tar cpvf "$2.tar"     "$2"    ;;
            bz2)    bzip "$2"                     ;;
            gz)     gzip -c -9 -n "$2" >  "$2.gz" ;;
            zip)    zip -r "$2.zip" "$2"          ;;
            7z)     7z a "$2".7z "$2"             ;;
            *)      echo "'$1' cannot be packed via $0" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# color man pages
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
        man "$@"
}

HISTFILE=~/.zhistory
HISTSIZE=3000
SAVEHIST=3000
setopt APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS # remove blank lines from history file
# if the command starts with a whitespace, don't add it to history
setopt HIST_IGNORE_SPACE
# append every single command to $HISTFILE immediately after hitting ENTER
setopt INC_APPEND_HISTORY
# history sharng is needed too rarely to keep it enabled. Instead use "fc -R" to read history file when you need to enter command from other termianl.
unsetopt SHARE_HISTORY

hash colordiff 2>/dev/null && alias diff='colordiff'

# don't try to correct typos
alias mkdir='nocorrect mkdir'
alias mv='nocorrect mv'

alias ed='vim'

fixlayout-with-prebuffer() {
    TEMPCUTBUFFER=$(echo $CUTBUFFER)
    zle kill-whole-line # get current line into CUTBUFFER

    en="\`@#\$\^&qwertyuiop\[\]QWERTYUIOPasdfghjkl;\'ASDFGHJKL|zxcvbnm,ZXCVBNM?./"
    ru="ё\"№;:?йцукенгшщзхъЙЦУКЕНГШЩЗфывапролджэФЫВАПРОЛД/ячсмитьбЯЧСМИТЬ,ю."

    led=$(xset -q|grep LED| awk '{ print $10 }') # check indicator

    case $led in
        00000000)  f=$en && s=$ru  ;;
        00001000)  f=$ru && s=$en  ;;
    esac
    
    CUTBUFFER=$(echo $CUTBUFFER | sed y=$f=$s=)
    xkb-switch -n # switch next lang

    zle yank # print switched symbols into terminal input
    CUTBUFFER=$(echo $TEMPCUTBUFFER)
}
zle -N fixlayout-with-prebuffer
bindkey "^Z" fixlayout-with-prebuffer
